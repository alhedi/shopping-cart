package demo.alex.shoppingcart.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

/*
* The EventService will publish cart events. The current supported events are ADD and DELETE.
* Currently, it only logs the events.
 */
@Component
public class EventService {

    private static final Logger logger = LoggerFactory.getLogger(EventService.class);

    /**
     * Publish an ADD event.
     * @param id long
     * @param quantity int
     */
    public void publishAddEvent(long id,
                                int quantity) {
        logger.info(new CartEvent(CartEvent.CartEventType.ADD, id, quantity, new Date()).toString());
    }

    /**
     * Publish a DELETE event
     * @param id long
     * @param quantity int
     */
    public void publishDeleteEvent(long id,
                                   int quantity) {
        logger.info(new CartEvent(CartEvent.CartEventType.DELETE, id, quantity, new Date()).toString());
    }
}
