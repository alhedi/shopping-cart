package demo.alex.shoppingcart.controller;

/**
 * A CartRequest is sent to modify the cart.
 */
public class CartRequest {

    private String type;
    private long id;
    private int quantity;

    public CartRequest(String type, long id, int quantity) {
        this.type = type;
        this.id = id;
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
