package demo.alex.shoppingcart.service;

import demo.alex.shoppingcart.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * This service deals with different operations on a cart with products. The service only has a single cart.
 */
@Service
public class CartService {

    public static final int MAX_PRODUCT_COUNT = 1000;

    private double discount;
    private final List<CartItem> cart;
    private final EventService eventService;
    private final ProductRepository productRepository;

    @Autowired
    public CartService(EventService eventService,
                       ProductRepository productRepository) {
        discount = 1;
        cart = new ArrayList<>();
        this.eventService = eventService;
        this.productRepository = productRepository;
    }

    /**
     * Add a given quantity of a product with the given id to the cart.
     * @param id long
     * @param quantity int
     */
    public void addProduct(final long id,
                           final int quantity) {
        cart.stream()
                .filter(item -> item.getProduct().getId() == id)
                .findFirst()
                .ifPresentOrElse(item -> item.setQuantity(Math.min(item.getQuantity() + quantity, MAX_PRODUCT_COUNT)),
                        () -> productRepository.findById(id).ifPresent(product ->
                                cart.add(new CartItem(product, Math.min(quantity, MAX_PRODUCT_COUNT)))));

        eventService.publishAddEvent(id, quantity);
    }

    /**
     * Remove the given quantity of the product with the given id from the cart.
     * @param id long
     * @param quantity int
     */
    public void removeProduct(final long id,
                              final int quantity) {
        cart.removeIf(item -> item.getProduct().getId() == id && item.getQuantity() - quantity <= 0);
        cart.stream()
                .filter(item -> item.getProduct().getId() == id)
                .findFirst()
                .ifPresent(item -> item.setQuantity(item.getQuantity() - quantity));

        eventService.publishDeleteEvent(id, quantity);
    }

    /**
     * Set the discount for the cart.
     * @param discount double
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * Get the current discount.
     * @return double
     */
    public double getDiscount() {
        return this.discount;
    }

    /**
     * Get the total price for all the products in the cart.
     * @return String
     */
    public String getPrice() {
        BigDecimal price = cart.stream()
                .map(item -> item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .multiply(BigDecimal.valueOf(discount))
                .setScale(2, RoundingMode.DOWN);

        return price.toPlainString();
    }

    /**
     * Get the cart
     * @return List
     */
    public List<CartItem> getCart() {
        return cart;
    }

    /**
     * Clear the cart of items.
     */
    public void clearCart() {
        cart.forEach(item -> eventService.publishDeleteEvent(item.getProduct().getId(), item.getQuantity()));
        cart.clear();
    }

}
