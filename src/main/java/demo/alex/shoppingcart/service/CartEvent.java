package demo.alex.shoppingcart.service;

import java.util.Date;

/**
 * A CartEvent contains information of cart events, currently ADD and DELETE operations.
 */
public class CartEvent {

    enum CartEventType {
        ADD,
        DELETE;
    }

    private final CartEventType eventType;
    private final long productId;
    private final int quantity;
    private final Date timestamp;

    public CartEvent(CartEventType eventType,
                     long productId,
                     int quantity,
                     Date  timestamp) {
        this.eventType = eventType;
        this.productId = productId;
        this.quantity = quantity;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "CartEvent{" +
                "eventType=" + eventType +
                ", productId=" + productId +
                ", quantity=" + quantity +
                ", timestamp=" + timestamp +
                '}';
    }
}
