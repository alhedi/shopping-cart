# The Shopping Cart #

This is a super simple shopping cart microservice that emulates a single shopping cart. 
The app exposes an API which can be used to perform actions on the one, and only, shopping cart.

## Requirements ##

* Java 17
* Maven 3.x
* Postman to use postman collection

## How to run ##

The application can be started from the commandline with the following maven command:

````
mvn spring-boot:run
````

The applicaiton will start on port 8080.

Upon starting Flyweight will run the initial DB migration script to intialize the in-memory H2 DB which contains a few products.

## API ##

The application exposes a REST API that consumes and produces JSON. Every endpoint returns the current state of the 
cart.

### Endpoints ###

---
**Request:**
````
GET /api/cart
````
Get the cart with its content, price and discount.

---
**Request:**
````
POST /api/discount
````
POST a discount for the cart.

**Request body example:**
````
{
    "discount":"0.5
}
````

---
**Request:**
````
POST /api/cart
````
Post a modification of the cart.

**Request body example:**
````
{
    "type":"ADD",
    "id":"4",
    "quantity":"8"
}
````

**type** is the type of modification to the cart.

Available types for modification are:

* ADD
* REMOVE
* CLEAR <- the CLEAR type will wipe the cart and does not require the **id** or **quantity** to be present.

**id** is the id of the product.

**quantity** is the quantity of the product to add or remove.

---

**Response example:**
````
{
    "cartItems": [
        {
            "product": {
                "id": 4,
                "name": "puppy",
                "price": 2499.0
            },
            "quantity": 16
        },
        {
            "product": {
                "id": 2,
                "name": "laser sword",
                "price": 125.5
            },
            "quantity": 3
        }
    ],
    "discount": 0.34,
    "price": "13722.57"
}
````