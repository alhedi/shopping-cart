CREATE TABLE PRODUCT (
   id LONG NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(50) NOT NULL,
   price REAL NOT NULL
);

INSERT INTO PRODUCT (name, price) VALUES ('toilet paper', 10.0);
INSERT INTO PRODUCT (name, price) VALUES ('laser sword', 125.50);
INSERT INTO PRODUCT (name, price) VALUES ('pen', 1.50);
INSERT INTO PRODUCT (name, price) VALUES ('puppy', 2499);
INSERT INTO PRODUCT (name, price) VALUES ('bicycle', 199.99);
INSERT INTO PRODUCT (name, price) VALUES ('map', 99);
INSERT INTO PRODUCT (name, price) VALUES ('parachute', 2540.50);
INSERT INTO PRODUCT (name, price) VALUES ('lamp', 149);
INSERT INTO PRODUCT (name, price) VALUES ('couch', 2499);
INSERT INTO PRODUCT (name, price) VALUES ('carpet', 1499);
INSERT INTO PRODUCT (name, price) VALUES ('computer screen', 3499);
