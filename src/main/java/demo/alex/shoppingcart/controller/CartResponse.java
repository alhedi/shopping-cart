package demo.alex.shoppingcart.controller;

import demo.alex.shoppingcart.service.CartItem;

import java.util.List;

public class CartResponse {

    private List<CartItem> cartItems;
    private double discount;
    private String price;

    public CartResponse(List<CartItem> cartItems,
                        double discount,
                        String price) {
        this.cartItems = cartItems;
        this.discount = discount;
        this.price = price;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
