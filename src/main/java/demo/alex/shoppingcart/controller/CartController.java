package demo.alex.shoppingcart.controller;

import demo.alex.shoppingcart.service.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class CartController {

    private static final Logger logger = LoggerFactory.getLogger(CartController.class);

    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    /**
     * Get the current cart.
     * @return CartResponse
     */
    @GetMapping(value = "/cart", produces = "application/json")
    public CartResponse getCart() {
        return new CartResponse(cartService.getCart(), cartService.getDiscount(), cartService.getPrice());
    }

    /**
     * Set the discount for the cart. Current discount is returned to the requester.
     * @param discount double
     * @return String
     */
    @PostMapping(value = "/discount", consumes = "application/json", produces = "application/json")
    public CartResponse setDiscount(@RequestBody DiscountRequest discount) {
        cartService.setDiscount(discount.getDiscount());

        return new CartResponse(cartService.getCart(), cartService.getDiscount(), cartService.getPrice());
    }

    /**
     * Modify the cart: ADD items, DELETE items, or CLEAR cart. Returns a CartResponse with full inventory of the cart
     * and its price.
     * @param cartRequest CartRequest
     * @return CartResponse
     */
    @PostMapping(value = "/cart", consumes = "application/json", produces = "application/json")
    public CartResponse modifyCart(@RequestBody CartRequest cartRequest)  {
        String type = cartRequest.getType() == null ? "" : cartRequest.getType();

        switch (type) {
            case "ADD" -> cartService.addProduct(cartRequest.getId(), cartRequest.getQuantity());
            case "REMOVE" -> cartService.removeProduct(cartRequest.getId(), cartRequest.getQuantity());
            case "CLEAR" -> cartService.clearCart();
            default -> logger.info("Invalid request for modifyCart: {}", cartRequest);
        }

        return new CartResponse(cartService.getCart(), cartService.getDiscount(), cartService.getPrice());
    }

}
