package demo.alex.shoppingcart.controller;

import com.fasterxml.jackson.annotation.JsonCreator;

public class DiscountRequest {

    private double discount;

    @JsonCreator
    public DiscountRequest(double discount) {
        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
